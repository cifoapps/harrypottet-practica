import 'package:fitness_time/constants.dart';
import 'package:fitness_time/ui/widget/activities_list.dart';
import 'package:fitness_time/ui/widget/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

class FitnessHome extends StatelessWidget {
  const FitnessHome({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    const url = "https://randomuser.me/api/portraits/women/44.jpg";
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: const CircleAvatar(
                radius: 48.0,
                backgroundImage: NetworkImage(url),
              ),
            ),
          ],
          leading: const Icon(Icons.menu_sharp),
          title: const Center(
            child: Text(
              "Fitness home",
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: theme.colorScheme.background,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Hola Diana",
                    style: theme.textTheme.headlineMedium,
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    "Come 5 veces al dia y permanece hidratada durante el día",
                    style: theme.textTheme.bodyMedium,
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  InkWell(
                    child: const Text(
                      "Más detalles",
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                    onTap: () {
                      //TODO: Details Screen
                    },
                  ),
                  const SizedBox(
                    height: 32.0,
                  ),
                  Text("Últimas actividades",
                      style: theme.textTheme.titleSmall),
                  const SizedBox(
                    height: 16.0,
                  ),
                  ActivitiesColumn(userId: 1),
                  Center(
                    child: Container(
                      padding: const EdgeInsets.all(8.0),
                      color: theme.colorScheme.background,
                      child: CircularPercentIndicator(
                        radius: 64.0,
                        lineWidth: 15.0,
                        animation: true,
                        percent: 0.7,
                        center: const Text(
                          "70.0%",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 23.0,
                          ),
                        ),
                        footer: const Text(
                          "Objetivo mensual",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0,
                          ),
                        ),
                        circularStrokeCap: CircularStrokeCap.round,
                        progressColor: kMajoreleBlue,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: const BottomNavBar(),
        drawer: null,
      ),
    );
  }
}
