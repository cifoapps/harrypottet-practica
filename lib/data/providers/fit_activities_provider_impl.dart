import 'package:fitness_time/domain/fit_activities_provider.dart';

class FitnessProviderImpl implements FitnessProvider {

   final List<Map<String, String>> _activitiesList = [
    {'activity': "Running", "date": "Yer, 14:23", "distance": "4,443"},
    {'activity': "Running", "date": "13-11-23", "distance": "5,43"},
    {'activity': "Running", "date": "12-11-23", "distance": "1,43"},
  ];

 @override
  List<Map<String, String>> get activitiesList => _activitiesList;
  
}