import 'dart:ui';

const Color kMindaro = Color(0xFFedf67d);
const Color kPersianPink = Color(0xFFF896D8);
const Color kHeliotrope = Color(0xFFCA7DF9);
const Color kMajoreleBlue = Color(0xFF724CF9);
const Color kUltraViolet = Color(0xFF564592);
const Color kWhite = Color(0xFFfff4fb);
const Color kVeryLightGrey = Color(0xFFF2F2F2);
