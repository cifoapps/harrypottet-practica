import 'package:fitness_time/ui/screen/fitness_home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'constants.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: fitnessLightTheme,
        home: const FitnessHome());
  }
}

ThemeData fitnessLightTheme = ThemeData(
    useMaterial3: false,
    appBarTheme: const AppBarTheme(
      color: kPersianPink,
      titleTextStyle: TextStyle(
        fontSize: 25,
        color: Colors.white,
      ),
      iconTheme: IconThemeData(
        color: kWhite,
      ),
    ),
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.white,
      // background: const Color(0xFFF2F2F2),
    ),
    fontFamily: GoogleFonts.montserrat().fontFamily);
